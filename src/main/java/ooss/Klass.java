package ooss;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
public class Klass {

    private int number;
    private Student leader;
    private List<Person> observers = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public void assignLeader(Student leader) {
        if(leader.isIn(this)) {
            this.leader = leader;
            notifyObservers();
        }else {
            System.out.println("It is not one of us.");
        }
    }

    public void attach(Person person){
        if(!observers.contains(person)) observers.add(person);
    }

    private void notifyObservers() {
        for (Person person : observers) {
            person.update(this);
        }
    }

    public boolean isLeader(Student student) {
        return !Objects.isNull(leader) && leader.equals(student);
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass that = (Klass) o;
        return this.number == that.number;
    }



}
