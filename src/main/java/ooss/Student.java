package ooss;

import lombok.Getter;

import java.util.Objects;

@Getter
public class Student extends Person implements Observer{

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public Student() {}

    public String introduce(){
        String introduction = String.format("My name is %s. I am %d years old. I am a student.", name, age);
        if (!Objects.isNull(klass)){
            introduction = klass.isLeader(this)? introduction.concat(String.format(". I am the leader of class %d.", klass.getNumber()))
                    : !Objects.isNull(klass)? introduction.concat(String.format(" I am in class %d.", klass.getNumber())) : introduction;
        }
        return introduction;

    }

    @Override
    public void update(Klass klass) {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", name, klass.getNumber(), klass.getLeader().getName()));
    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return !Objects.isNull(this.klass) && this.klass.equals(klass);
    }
}
