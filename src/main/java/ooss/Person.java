package ooss;

import lombok.Getter;
//TODO: Getter is not suitable here
@Getter
public class Person implements Observer{
    //TODO: use private will be better
    protected int id;
    protected String name;
    protected int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Person() {}

    public String introduce(){
        return String.format("My name is %s. I am %d years old.", name, age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person that = (Person) o;
        return this.id == that.id;
    }

    public void update(Klass klass) {}
}
