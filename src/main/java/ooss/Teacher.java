package ooss;

//TODO: remove the unused import
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@Getter
public class Teacher extends Person {

    private List<Klass> klass = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public Teacher() {
    }

    public String introduce(){
        String introduction = String.format("My name is %s. I am %d years old. I am a teacher.", name, age);
        if(!klass.isEmpty()){
            introduction = introduction.concat(String.format(" I teach Class %s.", String.join(", ",klass.stream().map(i -> String.valueOf(i.getNumber())).toArray(String[]::new))));
        }
        return introduction;
    }

    public void assignTo(Klass klass) {
//        TODO: better naming
        if(this.klass.stream().noneMatch(i -> i.equals(klass))) {
            this.klass.add(klass);
        }
    }

    @Override
    public void update(Klass klass) {
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", name, klass.getNumber(), klass.getLeader().getName()));
    }

    public boolean belongsTo(Klass klass){
        return !this.klass.isEmpty() && !Objects.isNull(klass) && this.klass.stream().anyMatch(i -> i.equals(klass));
    }

    public boolean isTeaching(Student student) {
        return belongsTo(student.getKlass());
    }


}
